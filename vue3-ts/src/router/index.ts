import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'

// vue-router the root path is home. Under the root path there are three son paths, like books, users and books4user.
// Meanwhile there are three paths, which are parallel to the root path. They are register, login and library
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    children:[
      {
        path:"books",
        name:"books",
        meta:{
          isshow:true,
          title:"books",
        },
        component:()=> import('../views/BooksView.vue')
      },
      {
        path:"users",
        name:"users",
        meta:{
          isshow:true,
          title:"users",
        },
        component:()=> import('../views/UsersView.vue')
      },
      {
        path:"books4user",
        name:"books4user",
        meta:{
          isshow:true,
          title:"booksorder",
        },
        component:()=> import('../views/BooksView4User.vue')
      }
    ]
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "register" */ '../views/RegisterView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/LoginView.vue')
  },
  {
    path: '/library',
    name: 'library',
    component: () => import(/* webpackChunkName: "library" */ '../views/LibraryView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// a interceptor of router. when browser visit directories, /books, /users or /book4use, vue.js should check localstorage. 
// if the localstorage does not contain token, which is JWT, the browser would jump back to /login
 
router.beforeEach((to,from,next)=>{
  const token:string | null = localStorage.getItem('token')
  if(!token && to.path !== '/login' && to.path !== '/library' && to.path !== '/register'){
    next('/login')
  }else{
    next()
  }
})

export default router
