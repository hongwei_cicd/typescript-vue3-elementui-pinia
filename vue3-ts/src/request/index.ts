import axios from 'axios'

// the main interface in order to get different backend data

const service = axios.create(
    {
        baseURL: "http://127.0.0.1:3000",
        timeout: 5000,
        headers:{
            "Content-Type": "application/json; charset=utf-8"
        }
    }
)

export default service