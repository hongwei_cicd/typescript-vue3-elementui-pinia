import service from '.';

interface LoginData{
    username:string,
    password:string
}

// interface userInfoInt{
//     username:string
// }

interface userOrderInfoInt{
    title:string,
    quantity:string
}

interface booksInfoInt{
    title:string,
    author:string,
    quantity:string
}

interface booksRmvInt{
    title:string
}

interface booksUpdateInt{
    previous:{
        title:string
    },
    current:{
        title?:string,
        author?:string,
        quantity?:string
    }

}

// set up jwt token
let accessToken: string | null = null;

// POST /auth/login
// { "username", "password" }

export function login(data:LoginData) {
    return service({
        url:"/auth/login",
        method:"post",
        data,
    }).then((res)=>{
        accessToken = res.data.accessToken;
        return res;
    });
}

// POST /auth/register
// { "username", "password"}

export function register (data:LoginData){
    return service({
        url:"/auth/register",
        method:"post",
        data
    }).then((res)=>{
        return res;
    })
}

// GET /library/profile

export function profile(){
    const token = accessToken
    return service({

        url:"/library/profile",
        method:"get",
        headers:{
            Authorization:`Bearer ${token}`
        }

    }).then((res)=>{
        return res;
    })
}

// GET /library/books
// this api is without jwt for the sake of guest users

export function getBooks(){
    return service({
        url:"/library/books",
        method:"get"
    })
}

// this api is to take into account JWT

export function getBooksWjwt(){
    const token = accessToken
    return service({
        url:"/library/books",
        method:"get",
        headers:{
            Authorization:`Bearer ${token}`
        }
    
    })
}

// POST /library/user/books
// { "title", "quantity" }

export function orderBooksWjwt(data:userOrderInfoInt){
    const token = accessToken
    return service({
        url:"/library/user/books",
        method:"post",
        data,
        headers:{
            Authorization:`Bearer ${token}`
        }
    })
}

// POST /admin/books 
// { "Author", "Title", "Quantity"}

export function addBooksWjwt(data:booksInfoInt){
    const token = accessToken
    return service({
        url:"/admin/books",
        method:"post",
        data,
        headers:{
            Authorization:`Bearer ${token}`
        }
    })
}

// DELETE /admin/books 
// { "Title" }

export function delBooksWjwt(data:booksRmvInt){
    const token = accessToken
    return service({
        url:"/admin/books",
        method:"delete",
        data,
        headers:{
            Authorization:`Bearer ${token}`
        }
    })
}

// PUT /admin/books 
// { "previous", "current" }

export function updateBooksWjwt(data:booksUpdateInt){
    const token = accessToken
    return service({
        url:"/admin/books",
        method:"put",
        data,
        headers:{
            Authorization:`Bearer ${token}`
        }
    })
}

// DELETE /admin/users 
// {"username"}

export function delUsers(username:string){
    const token = accessToken
    const data = {
        username: username
    }
    return service({
        url:"/admin/users",
        method:"delete",
        data,
        headers:{
            Authorization: `Bearer ${token}`,
        }
    })

}

// GET /admin/users

export function getUsers() {

    const token = accessToken
    return service({
        url:"/admin/users",
        method:"get",
        headers:{
            Authorization: `Bearer ${token}`,
        },
    })
    
}

// PUT /admin/users 
// {"username"}

export function putUser(username:string){

    const token = accessToken
    const data = {
        username:username
    }
    return service({
        url:"/admin/users",
        method:"put",
        data,
        headers:{
            Authorization: `Bearer ${token}`,
        },
    })
}

