import { defineStore } from "pinia";
import { profile } from "../request/api";

// useusernameInfo is a function, which aims to return the share data
export const useUsernameInfo = defineStore('useInfo',{
    state: ()=>({
        info:""

    }),
    actions:{
        async post(){
            // const data = profile().then((res)=>{

            //     Object.assign(this.username, res.data.user)

            // })
            const res = await profile();
            this.info = res.data.user.username;
            console.log(this.info)
        }
    }
})