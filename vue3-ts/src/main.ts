import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import   'element-plus/dist/index.css'
import { createPinia } from 'pinia';

// import package include router, element-plus and pinia

createApp(App).use(router).use(ElementPlus).use(createPinia()).mount('#app')
