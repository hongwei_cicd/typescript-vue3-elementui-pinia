// define the information of book

export interface booksInt{
    title: string,
    author: string,
    quantity: string,
    orderNum:string
}

export interface libraryBooksInt{
    version:number
}

interface selectDataInt{
    title: string
}

interface activeInt{
    title: string,
    author: string,
    quantity: string
}

interface updateInt{
    previous: {
        title:string
    },
    current:{
        title:string,
        author:string,
        quantity:string
    }

}

interface orderInt{
    title: string,
    quantity: string
}

interface rmvInt{
    title: string
}

export class booksData{
    selectData:selectDataInt={
        title:""
    }
    libraryBooks:libraryBooksInt = {
        version: -1
    }

    list:booksInt[] = []

    isShow=false
    isShowUpdated=false
    isShowOrder=false
    isAddDialog=true

    active:activeInt={
        title:"",
        author:"",
        quantity:""
    }

    orderData:orderInt = {
        title:"",
        quantity:""
    }
    updateData:updateInt = {

        previous:{
            title:""
        },
        current:{
            title:"",
            author:"",
            quantity:""
        }
    }

    delData:rmvInt = {
        title:""
    }
}
        