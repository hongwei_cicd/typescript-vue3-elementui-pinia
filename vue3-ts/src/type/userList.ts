// import { GetStartIndexForOffset } from "element-plus"
// define the information of user

export interface userInt{
    username:string,
    password:string,
    role:string,
    purchasesNum:number
    purchases:purchasesInt[]
}

export interface selectDataInt{
    username:string
}

export interface libraryUsersInt{
    version:number
}

export interface purchasesInt{
    title:string,
    author:string,
    quantity:string

}


export interface activeInt{
    username:string,
    password:string,
    role:string
}

interface roleListInt{
    role:string
}

export class usersData{
    selectData:selectDataInt={
        username:""
    }
    libraryUsers:libraryUsersInt = {
        version: -1
    }
    list:userInt[] = []
    isShow=false
    isAddDialog=true
    rolelist:roleListInt[] = []
    active:activeInt={
        username:"",
        password:"",
        role:"",
    }
}