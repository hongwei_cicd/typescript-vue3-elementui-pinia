# Kursprojekt - Bookster
## Hongwei Han
Deadline: 16:a juni 17:00.

## Front-end (vue3, webpacket, vue-router, pinia)

1. visit the directory vue3-ts and run 
``` npm run serve ```
so you can get a result from vue.js
![image 1](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2001.png)

2. Use browser to visit local or network website, so that they can get a login website
``` http://localhost:8080/login```
![image 2](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2002.png)

3. if users log in the management system as admin, they can enter the home page. The username should be shown on the right top and localstorage saves jwt code.
![image 3](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2003.png)

4. when admins visit the books button at left side, they can get all information of books
![image 4](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2004.png)

5. when admins click the button, Add, a dialog should be popped up.
![image 5](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2005.png)

6. after fill in the dialog, the table should be updated.
![image 6](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2006.png)

7. if admins click the button, update, there is a new dialog. 
![image 7](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2007.png)
![image 8](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2008.png)

8. if admins click the button,order, there is a new dialog
![image 9](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2009.png)
![image 10](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2010.png)

9. The updated order information
![image 11](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2011.png)

10. when admins click the button, users, they would check users' information
![image 12](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2012.png)

11. admins can click the button update in order to change user's role
![image 13](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2013.png)
![image 14](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2014.png)

12. admins can delete a user
![image 15](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2015.png)

13. when people visit the website, register, they can register a new account as a user.
![image 16](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2016.png)

14. after login, users can enter the home page and users' name is list on the top right
![image 17](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2017.png)

15.  the user visit the booksorder page, 
![image 18](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2018.png)

16. users can order books as they want. the number of available books would be changed after the users confirm.
![image 19](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2019.png)
![image 20](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2020.png)

17. admins can check how many books the users order
![image 21](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2021.png)

18. people can also visit the management system without login
![image 22](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2022.png)


19. they can also search book's inform by inputting the book's name
![image 23](https://gitlab.com/hongwei_cicd/typescript-vue3-elementui-pinia/-/raw/main/img/Screenshot%2022.png)

20. I applied vue-route to reject a random person, who do not register in the system

21. after lick log out, the vue-route should jump into the login page

## Backend Api
### 1. Som gäst-, authentiserad- och administratöranvändare
<details>
  <summary>GET /library/books</summary>

  <p>Responsen är en lista över böckerna och ett verisonsnummer.</p>
  <hr>
</details>

<details>
  <summary>
    GET /library/books/search?q=query 
  </summary>
  <p>Query paramtern motsvarar en sökt boktitel. Observera att den skickas som en del av URL:n, /library/books/search?q=., ex. /library/books/search?q=night <-- söker bok med "night" i titeln</p>
  <br>
  <p>Responsen är en lista med matchade böcker och ett verisonsnummer.</p>
  <p>Vid tom "titel" så skickas alla böcker tillbaks som svar.</p>
  <hr>
</details>

<details>
  <summary>
    POST /auth/login
    { "username", "password" }
  </summary>
  <p>Username och password motsvarar giltiga inloggningsuppgifter.</p>

  <p>Vid lyckat inlogg så ges ett JWT token tillbaka som är giltigt i 45 minuter.</p>

  <p>Kan ge 403 om username eller password inte skickas med.</p>
  <p>Kan ge 403 om användaren inte anger giltiga uppgifter.</p>
  <p>Kan ge 403 om användaren inte finns i databasen.</p>
  <hr>
</details>

<details>
  <summary>
    POST /auth/register
    { "username", "password"}
  </summary>
  <p>Username är unikt och kan endast förekomma en gång.</p>

  <p>Vid lyckad registering så ges ett 201 statuskod tillbaka för att indikera att kontot är skapat.</p>

  <p>Kan ge 403 om username eller password inte skickas med.</p>
  <p>Kan ge 403 om användaruppgifterna redan finns.</p>
  <hr>
</details>
<hr>

### 2. Som authentiserad- och administratöranvändare
Samtliga anrop ger:
- 403 om authorized headern inte finns
- 403 om jwt inte kan verifieras
  
<details>
  <summary>
    GET /library/profile
  </summary>

  <p>Hämtar nuvarande profil för inloggad användare, info hämtas från jwt token:et. Här går det även att se köpta produkter.</p>
  <hr>
</details>

<details>
  <summary>
    POST /library/user/books
    { "title", "quantity" }
  </summary>
  <p>"title" är case sensitive.</p>
  <p>"quantity" godtas endast om antal böcker finns i databasen.</p>
  <br>
  <p>Responsen är en lista över alla böcker och ett verisonsnummer.</p>
  <br>
  <p>Vid avsaknad av paramterar ges 403.</p>
</details>
  <hr>
<hr>

### 3. Som administratöranvändare
Samtliga anrop ger:
- 401 om användaren inte har behörighet

<details>
  <summary>
  POST /admin/books { "Author", "Title", "Quantity"}
  </summary>

  <p>Ger statuskod 201 när en bok har lagts till.</p>
  <hr>
</details>

<details>
  <summary>
  PUT /admin/books { "previous", "current" }
  </summary>
  <p>Previous är en bok representerad med titel { "title" }</p>
  <p>Current är den nya boken representerad med den data som önskas uppdateras {"?title", "?author", "?quantity"}</p>
  <em>? = optional.</em>
  <br>
  <p>Ger statuskod 201 när en bok har uppdaterats.</p>
  <hr>
</details>

<details>
  <summary>
  DELETE /admin/books { "Title" }
  </summary>

  <p>Ger statuskod 200 när en bok har tagits bort</p>

  <p>Ger statuskod 404 om boken inte hittades</p>
  <hr>
</details>

<details>
  <summary>
  GET /admin/users
  </summary>

  <p>Ger en lista över alla användare</p>
  <hr>
</details>

<details>
  <summary>
  PUT /admin/users {"username"}
  </summary>
  <p>Tilldelar administratör status till användaren med angivet username. <em>Username är case sensitive.</em></p>
  <br>
  <p>Ger 200 om användare gick att uppdatera.</p>
  <p>Ger 403 om användarnamnet inte går att hitta.</p>
  <hr>
</details>

<details>
  <summary>
  DELETE /admin/users {"username"}
  </summary>
  <p>Username är case sensitive.</p>

  <p>Ger 200 om användare gick att ta bort.</p>

  <p>Ger 403 om användarnamnet inte går att hitta</p>
  <hr>
</details>
<hr>

